﻿(function ($) {
    $.fn.extend({
        gips: function (options) {
            var settings = $.extend({textAttr: 'text', placement: 'bottom'}, options);
            return this.each(function () {
                var control = $(this);
                var toolTip = $('<div class="gips-container" style="display:none;"><div class="gips-body' + '">' + control.attr(settings.textAttr) + '</div><span class="gips-arrow gips-arrow-wrapper gips-arrow-wrapper-' + settings.placement + '"><span class="gips-arrow gips-arrow-' + settings.placement + '"</span></span></div>');
                $("body").append(toolTip);
                control.css({'position':'relative'});
                var toolTipArrow = $('.gips-arrow-wrapper', toolTip);
                control.mouseenter(function () {
                    var position = control.offset();
                    var left = position.left;
                    var top = position.top;
                    if (settings.placement == 'top') {
                        left -= (toolTip.outerWidth() - control.width()) / 2;
                        top -= toolTip.outerHeight() - toolTipArrow.outerHeight() / 2;
                    } else if (settings.placement == 'bottom') {
                        left -= (toolTip.outerWidth() - control.width()) / 2;
                        top += control.height() + toolTipArrow.outerHeight() / 2;
                        toolTipArrow.css('top', -toolTip.outerHeight());
                    } else if (settings.placement == 'left') {
                        left -= (toolTip.outerWidth() + toolTipArrow.outerWidth() / 2);
                        toolTipArrow.css('top', -(toolTip.outerHeight() / 2) + control.outerHeight());
                        toolTipArrow.css('left',toolTipArrow.outerWidth());
                        top -= toolTip.outerHeight() / 2;
                    } else if (settings.placement == 'right') {
                        left += (control.outerWidth() + toolTipArrow.outerWidth() / 2);
                        toolTipArrow.css('top', -toolTip.outerHeight() / 2);
                        toolTipArrow.css('left',-toolTipArrow.outerWidth());
                        top -= toolTip.outerHeight() / 2 - control.outerHeight();
                    }
                    toolTip.css({ left: left , top: top });
                    toolTip.show();
                    control.css({'zIndex':1000});
                }).mouseleave(function () {
                    toolTip.hide();
                    control.css({'zIndex':999});
                });
            });
        }
    });
})(jQuery);